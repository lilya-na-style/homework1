package main;

public class Arrays {

    public static int getMinimal( int[] arr) {
       arr = new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1};
        int min = arr[0];

        for(int i = 0; i < arr.length; ++i) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }

        return min;
    }

    public static int getMaximal( int[] arr) {
       arr = new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1};
        int max = arr[0];

        for(int i = 0; i < arr.length; ++i) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        return max;
    }

    public static int getIndexOfMin( int[] arr) {
        arr = new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1};
        int min = arr[0];
        int index = 0;

        for(int i = 0; i < arr.length; ++i) {
            if (arr[i] < min) {
                min = arr[i];
                index = i;
            }
        }

        return index;
    }

    public static int getIndexOfMax(int[] arr) {
        arr = new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1};
        int max = arr[0];
        int index = 0;

        for(int i = 0; i < arr.length; ++i) {
            if (arr[i] > max) {
                max = arr[i];
                index = i;
            }
        }

        return index;
    }

    public static int addUnevenIndexItems(int[] mas) {
        mas = new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1};
        int min = mas[0];
        int sum = 0;

        for(int i = 0; i < mas.length; ++i) {
            if (i % 2 != 0) {
                sum += mas[i];
            }
        }

        return sum;
    }

    public static int [] reverseArray(int[] arr ) {
        arr = new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1};
        int i = 0;
        for( i = arr.length - 1; i >= 0; --i) {



            System.out.print(arr[i]);
           // return reverseArr;
        }
        return arr;

       // return 0;
    }

    public static int countUnevenNumbers( int[] arr) {
        arr = new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1};
        int min = arr[0];
        int count = 0;

        for(int i = 0; i < arr.length; ++i) {
            if (arr[i] % 2 != 0) {
                ++count;
            }
        }

        return count;
    }

    public static void changeArrayParts() {
        int[] arr = new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1};

        for(int i = (arr.length - 1) / 2; i >= 0; ++i) {
            if (i <= arr.length - 1) {
                System.out.print(arr[i] + " ");
            }
        }

        for(int j = 0; j < (arr.length - 1) / 2; ++j) {
            System.out.print(arr[j] + " ");
        }

    }

    public static void sortBubble() {
        int[] arr = new int[]{7, 2, 10, -50, 90, 12, -340, 11, 60};

        int i;
        int j;
        int temp;
        for(i = 0; i < arr.length; ++i) {
            for(j = i + 1; j < arr.length; ++j) {
                if (arr[i] > arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        int[] var4 = arr;
        temp = arr.length;

        for(j = 0; j < temp; ++j) {
            i = var4[j];
            System.out.print(i + " ");
        }

    }

    public static void sortSelect() {
        int[] arr = new int[]{7, 2, 10, -50, 90, 12, -340, 11, 60};

        int i;
        int pos;
        int min;
        for(i = 0; i < arr.length; ++i) {
            pos = i;
            min = arr[i];

            for(int j = i + 1; j < arr.length; ++j) {
                if (arr[j] < min) {
                    pos = j;
                    min = arr[j];
                }
            }

            arr[pos] = arr[i];
            arr[i] = min;
        }

        int[] var5 = arr;
        min = arr.length;

        for(pos = 0; pos < min; ++pos) {
            i = var5[pos];
            System.out.print(i + " ");
        }

    }

    public static void sortInsert() {
        int[] array = new int[]{9, 1, 4, 3, 2, 43, 11, 58, 22};
        int n = array.length;
        for (int j = 1; j < n; j++) {
            int key = array[j];
            int i = j-1;
            while ( (i > -1) && ( array [i] > key ) ) {
                array [i+1] = array [i];
                i--;
            }
            array[i+1] = key;
        }
        for(int i:array){
            System.out.print(i+" ");
        }
    }


















    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static int partition(int[] arr, int low, int high) {
        int pivot = arr[high];
        int i = low - 1;

        for(int j = low; j <= high - 1; ++j) {
            if (arr[j] < pivot) {
                ++i;
                swap(arr, i, j);
            }
        }

        swap(arr, i + 1, high);
        return i + 1;
    }






    static void quickSort(int[] arr, int low, int high) {
        if (low < high) {
            int pi = partition(arr, low, high);
            quickSort(arr, low, pi - 1);
            quickSort(arr, pi + 1, high);
        }

    }

    static void printArray(int[] arr, int size) {
        for(int i = 0; i < size; ++i) {
            System.out.print(arr[i] + " ");
        }

        System.out.println();
    }

    static void printArray(int[] arr) {
        int n = arr.length;

        for(int i = 0; i < n; ++i) {
            System.out.print(arr[i] + " ");
        }

        System.out.println();
    }

    static int sort(int[] arr) {
        int n = arr.length;

        for(int gap = n / 2; gap > 0; gap /= 2) {
            for(int i = gap; i < n; ++i) {
                int temp = arr[i];

                int j;
                for(j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                }

                arr[j] = temp;
            }
        }

        return 0;
    }

    void heapify(int[] arr, int n, int i) {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;
        if (l < n && arr[l] > arr[i]) {
            largest = l;
        }

        if (r < n && arr[r] > arr[largest]) {
            largest = r;
        }

        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
            this.heapify(arr, n, largest);
        }

    }

    void merge(int[] arr, int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;
        int[] L = new int[n1];
        int[] R = new int[n2];

        int i;
        for(i = 0; i < n1; ++i) {
            L[i] = arr[l + i];
        }

        for(i = 0; i < n2; ++i) {
            R[i] = arr[m + 1 + i];
        }

        i = 0;
        int j = 0;

        int k;
        for(k = l; i < n1 && j < n2; ++k) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                ++i;
            } else {
                arr[k] = R[j];
                ++j;
            }
        }

        while(i < n1) {
            arr[k] = L[i];
            ++i;
            ++k;
        }

        while(j < n2) {
            arr[k] = R[j];
            ++j;
            ++k;
        }

    }

    void sort(int[] arr, int l, int r) {
        if (l < r) {
            int m = l + (r - l) / 2;
            this.sort(arr, l, m);
            this.sort(arr, m + 1, r);
            this.merge(arr, l, m, r);
        }

    }
}

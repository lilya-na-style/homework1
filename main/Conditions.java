package main;

public class Conditions {


    public static int multiplyOrAdd(int a, int b) {
        int result;
        if (a % 2 == 0) {
            result = a * b;
        } else {
            result = a + b;
        }

        return result;
    }

    public static int getQuarter(int x, int y) {
        int quarter = 0;
        if (x < 0 && y > 0) {
            quarter = 1;
        } else if (x > 0 && y > 0) {
            quarter = 2;
        } else if (x < 0 && y < 0) {
            quarter = 3;
        } else if (x > 0 && y < 0) {
            quarter = 4;
        }

        return quarter;
    }

    public static int addPositives(int x, int y, int z) {
        int sum;
        if (x > 0 && y > 0 && z > 0) {
            sum = x + y + z;
        } else if (x > 0 && y > 0 && z < 0) {
            sum = x + y;
        } else if (x > 0 && y < 0 && z > 0) {
            sum = x + z;
        } else if (x < 0 && y > 0 && z > 0) {
            sum = y + z;
        } else if (x < 0 && y < 0 && z > 0) {
            sum = z;
        } else if (x > 0 && y < 0 && z < 0) {
            sum = x;
        } else {
            sum = y;
        }

        return sum;
    }

    public static int countExpression(int a, int b, int c) {
        int sum = a + b + c;
        int product = a * b * c;
        if (sum > product) {
            sum += 3;
            return sum;
        } else {
            product += 3;
            return product;
        }
    }

    public static String getMark(int i) {
        String mark = " ";
        if (i != 0 && i > 19) {
            if (i != 20 && i > 39) {
                if (i != 40 && i > 59) {
                    if (i != 60 && i > 74) {
                        if (i != 75 && i > 89) {
                            if (i == 90 || i <= 100) {
                                mark = "A";
                            }
                        } else {
                            mark = "B";
                        }
                    } else {
                        mark = "C";
                    }
                } else {
                    mark = "D";
                }
            } else {
                mark = "E";
            }
        } else {
            mark = "F";
        }

        return mark;
    }
}


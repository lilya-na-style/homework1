package main;

public class Functions {
    long number = 444696542349L;
    long milliard;
    long million;
    long thousand;
    long hundred;
    int[] numbers;
    String[] writtennumbers;
    int i;
    int j;
    long x;
    long z;
    String hundreds;
    String tens;
    String ones;

    public Functions() {
        this.milliard = (this.number - this.number % 1000000000L) / 1000000000L;
        this.million = this.number % 1000000000L / 1000000L;
        this.thousand = this.number % 1000000L / 1000L;
        this.hundred = this.number % 1000L;
        this.numbers = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900};
        this.writtennumbers = new String[]{"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        this.hundreds = "";
        this.tens = "";
        this.ones = "";
    }

    public static String getDayOfWeek(int v) {
        String day = "";
        switch(v) {
            case 1:
                day = "Monday";
                break;
            case 2:
                day = "Tuesday";
                break;
            case 3:
                day = "Wednesday";
                break;
            case 4:
                day = "Thursday";
                break;
            case 5:
                day = "Friday";
                break;
            case 6:
                day = "Saturday";
                break;
            case 7:
                day = "Sunday";
        }

        return day;
    }

    public static double getDistanceBetweenTwoPoints(int [] arr1, int [] arr2) {
        arr1 = new int[] {6, 3};
        arr2 = new int[] {-1, 8};
        double x = Math.sqrt(Math.pow((double)(arr2[0] - arr1[0]), 2.0D) + Math.pow((double)(arr2[1] - arr1[1]), 2.0D));
           System.out.println(x);
        return x;

    }

    public static String getStringFromNumber(int number) {
        String words = "";
        if (number == 0) {
            return "zero";
        } else if (number < 0) {
            return "minus " + getStringFromNumber(Math.abs(number));
        } else {
            if (number / 1000000 > 0) {
                words = words + getStringFromNumber(number / 1000000) + " million";
                number %= 1000000;
            }

            if (number / 1000 > 0) {
                words = words + getStringFromNumber(number / 1000) + " thousand";
                number %= 1000;
            }

            if (number / 100 > 0) {
                words = words + getStringFromNumber(number / 100) + " hundred";
                number %= 100;
            }

            if (number > 0) {
                if (words != "") {
                    words = words + "and ";
                }

                String[] unitsMap = new String[]{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
                String[] tensMap = new String[]{"zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
                if (number < 20) {
                    words = words + unitsMap[number];
                } else {
                    words = words + tensMap[number / 10];
                    if (number % 10 > 0) {
                        words = words + "-" + unitsMap[number % 10];
                    }
                }
            }

            return words;
        }
    }

    public static String getString(int n) {

        int[] numbers = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900};
        String[] writtennumbers = new String[]{"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};

        for(int i = 0; i < numbers.length; ++i) {
            for(int j = 0; j < writtennumbers.length; ++j) {
                i = j;
                if (numbers[j] == n) {
                    System.out.println(writtennumbers[j]);
                } else {
                    int x;
                    if (n != numbers[j] && n > 99) {
                        x = n % 100;
                        if (n - x == numbers[j]) {
                            System.out.println(writtennumbers[j]);
                        }

                        if (x == numbers[j]) {
                            System.out.println(writtennumbers[j]);
                        } else {
                            int z = x % 10;
                            if (x - z == numbers[j]) {
                                System.out.println(writtennumbers[j]);
                            }

                            if (z != 0 && z == numbers[j]) {
                                System.out.println(writtennumbers[j]);
                            }
                        }
                    } else if (n != numbers[j] && n <= 99) {
                        x = n % 10;
                        if (n - x == numbers[j]) {
                            System.out.println(writtennumbers[j]);
                        }

                        if (x == numbers[j]) {
                            System.out.print(writtennumbers[j]);
                        }
                    }
                }
            }
        }

        return null;
    }

    public static int getNumberFromString(String str) {
        String[] arr = str.split(" ");

        int[] mas = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900};
        int sum = 0;
        String[] mas2 = new String[]{"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};



        for(int a = 0; a < arr.length; ++a) {
            for(int i = 0; i < mas2.length; ++i) {

                if (arr[a].equals(mas2[i])) {
                    for(int j = 0; j < mas.length; ++j) {
                        if (j == i) {
                            sum += mas[j];
                        }
                    }
                }
            }
        }

        System.out.println(sum);
        return sum;
    }
/*
    public void getStringFromBigNumber1() {
        for(this.i = 0; this.i < this.numbers.length; ++this.i) {
            for(this.j = 0; this.j < this.writtennumbers.length; ++this.j) {
                this.i = this.j;
                if ((long)this.numbers[this.i] == this.million) {
                    this.ones = this.writtennumbers[this.j];
                }

                if (this.million != (long)this.numbers[this.i] && this.million > 99L) {
                    this.x = this.million % 100L;
                    if (this.million - this.x == (long)this.numbers[this.i]) {
                        this.hundreds = this.writtennumbers[this.j];
                    }

                    if (this.x == (long)this.numbers[this.i]) {
                        this.ones = this.writtennumbers[this.j];
                    } else {
                        this.z = this.x % 10L;
                        if (this.x - this.z == (long)this.numbers[this.i]) {
                            this.tens = this.writtennumbers[this.j];
                        }

                        if (this.z != 0L && this.z == (long)this.numbers[this.i]) {
                            this.ones = this.writtennumbers[this.j];
                        }
                    }
                } else if (this.million != (long)this.numbers[this.i] && this.million <= 99L) {
                    this.x = this.million % 10L;
                    if (this.million - this.x == (long)this.numbers[this.i]) {
                        this.tens = this.writtennumbers[this.j];
                    }

                    if (this.x == (long)this.numbers[this.i]) {
                        this.ones = this.writtennumbers[this.j];
                    }
                }
            }
        }

        System.out.print(this.hundreds + " " + this.tens + " " + this.ones + " миллиарда(ов)");
        System.out.println(" ");
    }
*/

    public String getStringFromBigNumber(long m) {
        int[] numbers = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900};
        String[] writtennumbers = new String[]{"один", "два(е)", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        String hundreds = "";
        String tens = "";
        String ones = "";

        for(int i = 0; i < numbers.length; ++i) {
            for(int j = 0; j < writtennumbers.length; ++j) {
                i = j;
                if ((long)numbers[j] == m) {
                    ones = writtennumbers[j];
                }

                long x;
                if (m != (long)numbers[j] && m > 99L) {
                    x = m % 100L;
                    if (m - x == (long)numbers[j]) {
                        hundreds = writtennumbers[j];
                    }

                    if (x == (long)numbers[j]) {
                        ones = writtennumbers[j];
                    } else {
                        long z = x % 10L;
                        if (x - z == (long)numbers[j]) {
                            tens = writtennumbers[j];
                        }

                        if (z != 0L && z == (long)numbers[j]) {
                            ones = writtennumbers[j];
                        }
                    }
                } else if (m != (long)numbers[j] && m <= 99L) {
                    x = m % 10L;
                    if (m - x == (long)numbers[j]) {
                        tens = writtennumbers[j];
                    }

                    if (x == (long)numbers[j]) {
                        ones = writtennumbers[j];
                    }
                }
            }
        }

        System.out.print(hundreds + " " + tens + " " + ones);
        return hundreds + " " + tens + " " + ones;
    }
}
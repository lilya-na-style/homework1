package main;

public class Main {


    public static void main(String[] args) {
        System.out.println("Result: " + Conditions.multiplyOrAdd(4, 2));
        System.out.println("Quarter: " + Conditions.getQuarter(5, -2));
        System.out.println("Sum of positive numbers: " + Conditions.addPositives(-11, 10, 7));
        System.out.println("Expression result: " + Conditions.countExpression(6, 3, 2));
        System.out.println("Mark: " + Conditions.getMark(99));
        System.out.println(" ");
        System.out.println("Sum of even numbers: " + Loops.findEvenSumAndAmount(99));
        System.out.println("Number is prime: " + Loops.isNumberPrime(4));
        System.out.println("Square root 1: " + Loops.sqRootSeq(10));
        System.out.println("Square root 2: " + Loops.sqRootBinary(10));
        System.out.println("Factorial: " + Loops.getFactorial(8));
        System.out.println("Sum of number digits: " + Loops.getSum(8));
        System.out.println("Reverse number:");
        Loops.reverse(896);
        System.out.println(" ");

        System.out.println("Minimal in array: " + Arrays.getMinimal(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1}));
        System.out.println("Maximal in array: " + Arrays.getMaximal(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1}));
        System.out.println("Index of min: " + Arrays.getIndexOfMin(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1}));
        System.out.println("Index of max: " + Arrays.getIndexOfMax(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1}));
        System.out.println("Sum of uneven indexes items: " + Arrays.addUnevenIndexItems(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1}));
        System.out.println("Array reverse: ");
        Arrays.reverseArray(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1});
        System.out.println(" ");
        System.out.println("Amount of uneven array numbers: " + Arrays.countUnevenNumbers(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1}));
        System.out.println("Change array parts: ");
        Arrays.changeArrayParts();
        System.out.println(" ");
        System.out.println("Bubble sort: ");
        Arrays.sortBubble();
        System.out.println(" ");
        System.out.println("Selection sort: ");
        Arrays.sortSelect();
        System.out.println(" ");
        System.out.println("Insertion sort: ");
        Arrays.sortInsert();
        System.out.println(" ");

        System.out.println("Quick sort ");
        int[] arr = new int[]{1, 9, 4, 91, 14, 32};
        int n = arr.length;
        Arrays.quickSort(arr, 0, n - 1);
        Arrays.printArray(arr, n);
        int[] arr1 = new int[]{12, 4, 56, 2, 5};
        Arrays.sort(arr1);
        System.out.println("Shell sort: ");
        Arrays.printArray(arr1);
        int[] arr2 = new int[]{12, 33, 10, 7, 2, 1};
        Arrays.sort(arr2);
        System.out.println("Heap sort: ");
        Arrays.printArray(arr2);
        int[] arr3 = new int[]{67, 14, 34, 5, 4, 6};
        Arrays arrays = new Arrays();
        arrays.sort(arr3, 0, arr3.length - 1);
        System.out.println("Merge sort: ");
        Arrays.printArray(arr3);
        System.out.println(" ");

        System.out.println("Name of the day: " + Functions.getDayOfWeek(2));
        System.out.println("Distance: " );
        Functions.getDistanceBetweenTwoPoints(new int[] {6, 3}, new int[] {-1, 8});
        System.out.println("Number from string: ");
        Functions.getNumberFromString("двести сорок три");
        System.out.println("String from number: ");
        Functions.getString(789);
        System.out.println(" ");
        System.out.println("String from big number: ");
        long number = 444696542349L;
        long milliard = (number - number % 1000000000L) / 1000000000L;
        long million = number % 1000000000L / 1000000L;
        long thousand = number % 1000000L / 1000L;
        long hundred = number % 1000L;
        Functions f = new Functions();
        f.getStringFromBigNumber(milliard);
        System.out.print(" миллиарда(ов) \n");
        f.getStringFromBigNumber(million);
        System.out.print(" миллионов \n");
        f.getStringFromBigNumber(thousand);
        System.out.print(" тысячи \n");
        f.getStringFromBigNumber(hundred);
        System.out.println(" ");
      //  f.getStringFromBigNumber1();
    }
}
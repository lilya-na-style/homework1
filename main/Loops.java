package main;

public class Loops {


    public static int findEvenSumAndAmount(int i) {
        int sum = 0;
        int count = 0;

        for(i = 1; i <= 99; ++i) {
            if (i % 2 == 0) {
                sum += i;
                ++ count;
            }
        }

        System.out.println("Amount of even numbers: " + count);
        return sum;
    }

    public static boolean isNumberPrime(int number) {
        boolean response = true;

        for(int i = 2; i <= number; ++i) {
            if (number % i == 0) {
                response = false;
            }
        }

        return response;
    }

    public static int sqRootSeq(int key) {
        int i = 1;

        while (true) {
            int q = i * i;
            if (key == q) {
                return i;
            }

            if (key < q) {
                return i - 1;
            }

            ++i;
        }
    }

    public static int sqRootBinary(int key) {
        int min = 1;
        int max = key;
        int prev = 0;

        while(true) {
            int mid = (min + max) / 2;
            if (prev == mid) {
                return mid;
            }

            int q = mid * mid;
            if (key == q) {
                return mid;
            }

            if (key < q) {
                max = mid;
            } else {
                min = mid;
            }

            prev = mid;
        }
    }

    public static int getFactorial(int n) {
        int j = 1;

        for(int i = 1; i <= n; ++i) {
            j *= i;
        }

        return j;
    }

    public static int getSum(int n) {
        int j = 0;

        for(int i = 0; i <= n; ++i) {
            j += i;
        }

        return j;
    }

    public static int reverse(int n) {

        int reverseNum = 0;
        while(n != 0){
            reverseNum = reverseNum * 10 + n % 10;
            n = n / 10;

        }
        System.out.println(reverseNum);
        return reverseNum;

    }
}

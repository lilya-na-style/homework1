package test;
import main.Arrays;
import org.junit.Assert;
import org.junit.Test;

public class ArraysTest {

    Arrays cut = new Arrays();

    @Test
    public void getMinimalTest(){
        int result = cut.getMinimal(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1});
        Assert.assertEquals(-10, result);
    }

    @Test
    public void getMaximalTest(){
    int result = cut.getMaximal(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1});
    Assert.assertEquals(34, result);

    }

    @Test
    public void getIndexOfMinTest(){
    int result = cut.getIndexOfMin(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1});
    Assert.assertEquals(7, result);
    }

    @Test
    public void getIndexOfMaxTest(){
        int result = cut.getIndexOfMax(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1});
        Assert.assertEquals(6, result);


    }


    @Test
    public void addUnevenIndexItemsTest(){
        int result = cut.addUnevenIndexItems(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1});
        Assert.assertEquals(-6, result);

    }


    @Test
    public void reverseArrayTest() {
        int[] result = cut.reverseArray(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1});
      //  int[] ex = {1, -10, 34, 3, 0, -2, 10, 3, 5};
        int [] ex = {1, -10, 34, 3, 0, -2, 10, 3, 5};
         Assert.assertArrayEquals(ex, result);
    }

/*

    @Test
    public void countUnevenNumbersTest(){
        int result = cut.countUnevenNumbers(new int[]{5, 3, 10, -2, 0, 3, 34, -10, 1});
                Assert.assertEquals(4, result);

    }
*/

/*

    @Test
    public void changeArrayParts()
    @Test
    public void sortBubble()
    @Test
    public void sortSelect()
    @Test
    public void sortInsert()

     */
}

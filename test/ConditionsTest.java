package test;
import main.Conditions;
import org.junit.Assert;
import org.junit.Test;

public class ConditionsTest {

    Conditions cut = new Conditions();


    @Test
    public void multiplyOrAddTest() {

       int result = cut.multiplyOrAdd(7, 9);
        Assert.assertEquals(16, result);

    }

    @Test
    public void getQuarterTest() {
        int result = cut.getQuarter(2, -5);
        Assert.assertEquals(4, result);

    }



    @Test
    public void addPositivesTest(){
        int result = cut.addPositives(-11,10,7);
        Assert.assertEquals(17, result);

    }

    @Test
    public void countExpressionTest(){
        int result = cut.countExpression(4,2,6);
        Assert.assertEquals(51, result);
    }

    @Test
    public void getMarkTest(){
        String result = cut.getMark(78);
        Assert.assertEquals("B", result );
    }

}

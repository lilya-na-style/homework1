package test;
import org.junit.Assert;
import main.Loops;
import main.*;
import org.junit.Test;

public class LoopsTest {

    Loops cut = new Loops();

    @Test
    public void findEvenSumAndAmountTest(){
        int result = cut.findEvenSumAndAmount(99);
        Assert.assertEquals(2450, result); }


    @Test
    public void isNumberPrimeTest(){
        boolean result = cut.isNumberPrime(4);
        Assert.assertFalse(result);

    }
    @Test
    public void sqRootSeqTest(){
        int result = cut.sqRootSeq(10);
        Assert.assertEquals(3, result);

    }


    @Test
    public void sqRootBinaryTest(){
        int result = cut.sqRootBinary(10);
        Assert.assertEquals(3, result);

    }


    @Test
    public void getFactorialTest() {
        int result = cut.getFactorial(8);
        Assert.assertEquals(40320, result);
    }


    @Test
    public void getSumTest(){
        int result = cut.getSum(8);
        Assert.assertEquals(36, result);
    }

    @Test
    public void reverseTest(){
        int result = cut.reverse(896);
        Assert.assertEquals(698, result);

    }

}

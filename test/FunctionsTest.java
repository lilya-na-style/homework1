package test;
import main.Functions;
import org.junit.Assert;
import org.junit.Test;

public class FunctionsTest {

    Functions cut = new Functions();

    @Test

    public void getDayOfWeekTest(){
        String result = cut.getDayOfWeek(5);
        Assert.assertEquals("Friday", result);
    }
    @Test
    public void getDayOfWeekTest2(){
        String result = cut.getDayOfWeek(1);
        Assert.assertEquals("Monday", result);
    }
    @Test
    public void getDistanceBetweenTwoPointsTest(){
        double result = cut.getDistanceBetweenTwoPoints(new int[] {6, 3}, new int[] {-1, 8});
        Assert.assertEquals(8.602325267042627, result, -1.0);

    }

    @Test
    public void getNumberFromStringTest(){
        int result = cut.getNumberFromString("двести сорок три");
        Assert.assertEquals(243, result);
    }

    @Test
    public void getStringTest(){
        String result = cut.getString(789);
        String expected = "девять восемьдесят семьвот";
        Assert.assertEquals(expected, result);
    }

    @Test
    public void getStringFromBigNumberTest(){
        String result = cut.getStringFromBigNumber(444696542349L);
        Assert.assertEquals("четыреста сорок четыре миллиарда(ов) шестьсот девяносто шесть миллионов пятьсот сорок два(е) тысячи триста сорок девять", result);
    }

}
